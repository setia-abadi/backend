## How to setup and run

#### Setup project
```
$ git clone https://gitlab.com/setia-abadi/backend
$ cd backend
$ go mod tidy
```

#### Setup env
```
add conf.yml, copy example from conf.yml.example and fill the required value
```

### jwt key and migration and run
```
$ make jwt-key-gen
$ make migrate
$ make seed
$ go run .
```